package main

import (
	"fmt"
	"testing"

	log "github.com/Sirupsen/logrus"
	minio "github.com/minio/minio-go"
)

func TestBucketAPI(t *testing.T) {
	endpoint := "10.120.2.111:9000"
	accessKeyID := "AKIAIOSFODNN7EXAMPLE"
	secretAccessKey := "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
	useSSL := false

	// Initialize minio client object.
	minioClient, err := minio.New(endpoint, accessKeyID, secretAccessKey, useSSL)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println("____CLIENT:::___>>>>")
	log.Printf("%#v\n", minioClient) // minioClient is now setup

	// ---------------------------------------------
	fmt.Println("____BUCKETS:::___>>>>")
	buckets, err := minioClient.ListBuckets()
	if err != nil {
		fmt.Println(err)
		return
	}
	for _, bucket := range buckets {
		fmt.Println(bucket)
	}

	// ---------------------------------------------
	if true {
		fmt.Println("____SET POLICY____STARTS:::___>>>>")
		policy := `{"Version": "2012-10-17","Statement": [{"Action": ["s3:GetObject"],"Effect": "Allow","Principal": {"AWS": ["*"]},"Resource": ["arn:aws:s3:::central/*"],"Sid": ""}]}`
		err = minioClient.SetBucketPolicy("public", policy)
		if err != nil {
			fmt.Println(err)
			return
		}
	}

	fmt.Println("____GET POLICY____STARTS:::___>>>>")
	policy, err := minioClient.GetBucketPolicy("central")
	if err != nil {
		log.Fatalln(err)
	} else {
		fmt.Println(policy)
	}

	// ---------------------------------------------
	doneCh := make(chan struct{})
	// Indicate to our routine to exit cleanly upon return.
	defer close(doneCh)

	isRecursive := true
	objectCh := minioClient.ListObjectsV2("central", "", isRecursive, doneCh)
	fmt.Println("____OBJECTS____STARTS:::___>>>>")
	for object := range objectCh {
		if object.Err != nil {
			fmt.Println(object.Err)
			return
		}
		//publicUrl := minioClient.protocol + "//" + minioClient.host + ":" + minioClient.port + "/" + minioBucket + "/" + object.name
		fmt.Println(object.Key)
		//fmt.Println(object.name)
	}

	// http://10.120.2.110:9000/central/final_movie.mp4

	err = minioClient.FGetObject("mybucket", "myobject", "/tmp/myobject", minio.GetObjectOptions{})
	if err != nil {
		fmt.Println(err)
		return
	}
}
