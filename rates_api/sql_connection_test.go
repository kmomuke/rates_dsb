package main

import (
	"database/sql"
	"fmt"
	"strconv"
	"testing"

	log "github.com/Sirupsen/logrus"
)

func TestSum(t *testing.T) {
	numbers := []int{1, 2, 3, 4, 5}
	expected := 15
	actual := Sum(numbers)

	if actual != expected {
		t.Errorf("Expected the sum of %v to be %d but instead got %d!", numbers, expected, actual)
	}
}

func Sum(numbers []int) int {
	sum := 0
	// This bug is intentional
	for _, n := range numbers {
		sum += n
	}
	return sum
}

func TestConnection(t *testing.T) {
	conn, err := sql.Open("sqlserver", "sqlserver://rates:QWasZX12@10.64.16.12?database=DoscredoBankOnline&connection+timeout=30")
	if err != nil {
		log.Fatal("Open connection failed:", err.Error())
	}
	defer conn.Close()

	if true {
		rowsDate, err := conn.Query("SELECT ID, BranchID, Name, Address FROM dbo.Offices WHERE CloseDate IS NULL")
		if err != nil {
			panic(err)
		}
		defer rowsDate.Close()
		dateColums, err := rowsDate.Columns()
		if err != nil {
			panic(err)
		}
		fmt.Println(dateColums)
		offices := make([]*Office, 0)
		for rowsDate.Next() {

			var ID int
			var BranchID int
			var Name []uint8
			var Address []uint8

			ofice := new(Office)
			err := rowsDate.Scan(
				&ID,
				&BranchID,
				&Name,
				&Address,
			)
			if err != nil {
				panic(err)
			}
			ofice.ID = strconv.Itoa(ID)
			ofice.BranchID = strconv.Itoa(BranchID)
			ofice.Name = B2S(Name)
			ofice.Address = B2S(Address)

			offices = append(offices, ofice)
		}
		for _, office := range offices {
			fmt.Printf("ID: %s", office.ID)
			fmt.Printf("BranchID: %s", office.BranchID)
			fmt.Printf("Name: %s", office.Name)
			fmt.Printf("Address: %s", office.Address)
		}
	}

	rowsDate, err := conn.Query("SELECT DATEOD FROM dbo.SYSINFO")
	if err != nil {
		panic(err)
	}
	defer rowsDate.Close()
	dateColums, err := rowsDate.Columns()
	if err != nil {
		panic(err)
	}
	fmt.Println(dateColums)
	var DATEOD string
	for rowsDate.Next() {
		err := rowsDate.Scan(
			&DATEOD,
		)
		if err != nil {
			panic(err)
		}
	}
	fmt.Println(DATEOD)

	rows, err := conn.Query("DoscredoCustomization.GetActualCouses",
		sql.Named("OfficeID", 1107),
		sql.Named("CrossCurrencyID", 417),
		sql.Named("TypeID", 1),
		sql.Named("CheckDate", DATEOD))

	if err != nil {
		panic(err)
	}
	defer rows.Close()

	dd, err := rows.Columns()
	if err != nil {
		panic(err)
	}
	fmt.Println(dd)

	rates := make([]*DosRate, 0)

	for rows.Next() {

		var CurrencyID int64
		var BuyRate []uint8
		var SellRate []uint8
		var NBKRRate []uint8
		var RateType string

		dosRate := new(DosRate)
		//[CurrencyID BuyRate SellRate NBKR_Rate RateType]

		err := rows.Scan(
			&CurrencyID,
			&BuyRate,
			&SellRate,
			&NBKRRate,
			&RateType,
		)
		if err != nil {
			panic(err)
		}

		dosRate.CurrencyID = CurrencyID
		dosRate.BuyRate = B2S(BuyRate)
		dosRate.SellRate = B2S(SellRate)
		dosRate.NBKRRate = B2S(NBKRRate)
		dosRate.RateType = RateType

		rates = append(rates, dosRate)
	}

	for index, dosRate := range rates {
		fmt.Printf("------%d\n------", index)
		fmt.Printf("CurrencyID: %d", dosRate.CurrencyID)
		fmt.Printf("BuyRate: %s", dosRate.BuyRate)
		fmt.Printf("SellRate: %s", dosRate.SellRate)
		fmt.Printf("NBKRRate: %s", dosRate.NBKRRate)
		fmt.Printf("RateType: %s", dosRate.RateType)
	}

	fmt.Println("")
	fmt.Println("finish sqlserverFunc")
}
