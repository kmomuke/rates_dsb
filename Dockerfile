FROM golang:1.9

# Copy the local package files to the container’s workspace.
WORKDIR /go/src/bitbucket.org/kmomuke/sms_server/megacom
ADD . /go/src/bitbucket.org/kmomuke/sms_server/megacom
ADD main.go /go/src/bitbucket.org/kmomuke/sms_server/megacom/main.go

# Install our dependencies
RUN go get github.com/denisenkom/go-mssqldb 
RUN go get github.com/Sirupsen/logrus
RUN go get github.com/labstack/echo
RUN go get github.com/dgrijalva/jwt-go
RUN go get github.com/denisenkom/go-mssqldb 

RUN go install bitbucket.org/kmomuke/sms_server/megacom

# Port on starting golang server
EXPOSE 1310

ENTRYPOINT /go/bin/megacom

#CMD ["go", "run", "/go/src/bitbucket.org/kmomuke/rentau/api/server.go"]