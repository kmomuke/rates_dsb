package main

import (
	"encoding/binary"
	"math"
)

/*
		EXEC DoscredoCustomization.GetActualCouses @OfficeID = 1112,            -- int
	                                           @CrossCurrencyID = 417,     -- int
	                                           @TypeID = 1,              -- tinyint
	                                           @CheckDate = '2018-12-21' -- date
*/

// Айбек Доскредо, [24.12.18 15:31]
// MSSQL
// host: 10.64.16.140
// login: sa
// pass: QWsfsdagvdfsbhs4236!@
// dbName: DoscredoBankOnline30082018

// Айбек Доскредо, [24.12.18 15:33]
// Хранимая процедура: DoscredoCustomization.GetActualCouses
// Передаваемые параметры:
// @OfficeID = 0,                           -- int
// @CrossCurrencyID = 417,          -- int
// @TypeID = 1,                             -- tinyint
// @CheckDate = '2018-12-27'  -- date

// func soredProcedure() {
// 	conn, err := sql.Open("sqlserver", "sqlserver://sa:QWsfsdagvdfsbhs4236!@@10.64.16.140?database=DoscredoBankOnline30082018&connection+timeout=30")
// 	if err != nil {
// 		fmt.Printf("somenumber:%d\n", 1111)
// 		log.Fatal("Open connection failed:", err.Error())
// 	}
// 	defer conn.Close()

// 	var somenumber int64
// 	_, err = conn.ExecContext(context.Background(), "DoscredoCustomization.GetActualCouses",
// 		sql.Named("OfficeID", 1104),
// 		sql.Named("CrossCurrencyID", 417),
// 		sql.Named("TypeID", 1),
// 		sql.Named("CheckDate", "2019-01-02"),
// 		//sql.Named("CurrencyID", sql.Out{Dest: &somenumber}),
// 	)
// 	if err != nil {
// 		panic(err)
// 	}
// 	fmt.Printf("CurrencyID:%d\n", somenumber)
// }

// func mssqlFunc() {
// 	conn, err := sql.Open("mssql", "sqlserver://sa:QWsfsdagvdfsbhs4236!@@10.64.16.140?database=DoscredoBankOnline30082018&connection+timeout=30")
// 	if err != nil {
// 		fmt.Printf("somenumber:%d\n", 1111)
// 		log.Fatal("Open connection failed:", err.Error())
// 	}
// 	defer conn.Close()

// 	rows, err := conn.Query("DoscredoCustomization.GetActualCouses",
// 		sql.Named("OfficeID", 1104),
// 		sql.Named("CrossCurrencyID", 417),
// 		sql.Named("TypeID", 1),
// 		sql.Named("CheckDate", "2019-01-02"))

// 	if err != nil {
// 		panic(err)
// 	}
// 	defer rows.Close()

// 	for rows.Next() {
// 		var somenumber int64
// 		err := rows.Scan(
// 			&somenumber,
// 		)
// 		if err != nil {
// 			panic(err)
// 		}
// 		fmt.Printf("somenumber:%d\n", somenumber)
// 	}

// 	fmt.Println("finish mssqlFunc")
// }

// func selectFunc() {
// 	conn, err := sql.Open("sqlserver", "sqlserver://sa:QWsfsdagvdfsbhs4236!@@10.64.16.140?database=DoscredoBankOnline30082018&connection+timeout=30")
// 	if err != nil {
// 		fmt.Printf("somenumber:%d\n", 1111)
// 		log.Fatal("Open connection failed:", err.Error())
// 	}
// 	defer conn.Close()

// 	rows, err := conn.Query(`SELECT CustomerName FROM dbo.CustomersNames()`)
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer rows.Close()

// 	type Timeline struct {
// 		Content string
// 	}
// 	for rows.Next() {
// 		timeline := Timeline{}
// 		err = rows.Scan(&timeline.Content)
// 		if err != nil {
// 			panic(err)
// 		}
// 		fmt.Println(timeline)
// 	}
// 	err = rows.Err()
// 	if err != nil {
// 		panic(err)
// 	}
// 	fmt.Println("finish storedProcedure")
// }
// Float64frombytes 123
func Float64frombytes(bytes []byte) float64 {
	bits := binary.LittleEndian.Uint64(bytes)
	float := math.Float64frombits(bits)
	return float
}

// Float64bytes 123
func Float64bytes(float float64) []byte {
	bits := math.Float64bits(float)
	bytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(bytes, bits)
	return bytes
}
func unusedMethod() {
	// type Timeline struct {
	// 	Content string
	// }
	// rows, err := conn.Query(`SELECT CustomerName FROM dbo.CustomersNames()`)
	// if err != nil {
	// 	panic(err)
	// }
	// defer rows.Close()
	// for rows.Next() {
	// 	timeline := Timeline{}
	// 	err = rows.Scan(&timeline.Content)
	// 	if err != nil {
	// 		panic(err)
	// 	}
	// 	fmt.Println(timeline)
	// }
	// err = rows.Err()
	// if err != nil {
	// 	panic(err)
	// }
	// rows, err := conn.Query(`EXEC DoscredoCustomization.GetActualCouses (@OfficeID)`)
	// if err != nil {
	// 	panic(err)
	// }
	// defer rows.Close()
	// for rows.Next() {
	// 	timeline := Timeline{}
	// 	err = rows.Scan(&timeline.Content)
	// 	if err != nil {
	// 		panic(err)
	// 	}
	// 	fmt.Println(timeline)
	// }
	// err = rows.Err()
	// if err != nil {
	// 	panic(err)
	// }

	// ctx, cancel := context.WithCancel(context.Background())
	// defer cancel()

	// _, err = conn.ExecContext(ctx, "DoscredoCustomization.GetActualCouses",
	// 	sql.Named("OfficeID", 0),
	// 	sql.Named("CrossCurrencyID", 417),
	// 	sql.Named("TypeID", 1),
	// 	sql.Named("CheckDate", "2019-01-11"),
	// )

	// if err != nil {
	// 	panic(err)
	// }

	// // var rs mssql.ReturnStatus
	// // _, err := conn.ExecContext(ctx, "theproc", &rs)
	// // log.Printf("status=%d", rs)

	// stmt, err := conn.Prepare("select 1, 'abc'")

	// if err != nil {
	// 	log.Fatal("Prepare failed:", err.Error())
	// }
	// defer stmt.Close()

	// row := stmt.QueryRow()
	// var somenumber int64
	// var somechars string
	// err = row.Scan(&somenumber, &somechars)
	// if err != nil {
	// 	log.Fatal("Scan failed:", err.Error())
	// }
	// fmt.Printf("somenumber:%d\n", somenumber)
	// fmt.Printf("somechars:%s\n", somechars)

	// fmt.Printf("bye\n")
}
