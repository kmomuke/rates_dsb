package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := echo.New()

	passwords := make(map[string]string)

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	e.GET("/changepas", func(c echo.Context) error {
		rand.Seed(time.Now().UnixNano())
		chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
		length := 8
		var b strings.Builder
		for i := 0; i < length; i++ {
			b.WriteRune(chars[rand.Intn(len(chars))])
		}
		str := b.String()

		passwords["hr"] = str
		newPassword, _ := passwords["hr"]

		fmt.Printf("\n\nPasword: %s\n\n", newPassword)
		return c.String(http.StatusOK, newPassword)
	})

	e.GET("/getpas", func(c echo.Context) error {
		password, ok := passwords["hr"]
		if ok {
			return c.String(http.StatusOK, password)
		}
		return c.String(http.StatusOK, "nopassword")
	})

	e.Logger.Fatal(e.Start(":1600"))
}
